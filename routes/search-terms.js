var express = require('express');
var router = express.Router();
var SearchTerm = require('../models/search-term')

// Returns the top 10 most searched terms
router.get('/top10', function (req, res, next) {
    SearchTerm.aggregate([
        { "$group": { _id: "$name", count: { $sum: 1 } } },{$limit : 10 } ], function (err, result) {
        if (err) {
            return res.status(401).json({
                title: 'error occured',
                error: err
            })
        }
        res.status(201).json({
            title: 'top 10 words',
            obj: result
        });
    });
});

//Saves search terms in DB
router.post('/', function (req, res, next) {
    req.body['searchTerms'].split(' ').forEach(function (term) {
            var searchTerm = new SearchTerm({
                name:term
            });
            searchTerm.save(function (err, result) {
                if (err) {
                    return res.status(500).json({
                        title: 'Error saving data',
                        error: err
                    })
                }
                res.status(200).json({
                    title: 'Search terms saved~',
                    obj: result
                });
            });
        }
    );

});

module.exports = router;
